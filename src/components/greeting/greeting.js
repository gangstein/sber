import React, { Fragment } from "react";

const Greeting = () => {
	return (
		<Fragment>
			<h1>Hello world</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos eos mollitia perferendis recusandae sunt.
				Consequuntur dicta dolor esse eveniet fugit iusto laborum, nihil nostrum numquam quidem, repudiandae suscipit ut
				vel.</p>
		</Fragment>
	);
};

export default Greeting;
