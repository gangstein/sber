import React, { Component } from "react";
import setHandler           from "../../actions/setHandler";
import { connect }          from "react-redux";
import "./content.css";

class Content extends Component {
	componentWillMount() {
		if (!this.props.item.done) {
			this.props.uploadServices(this.props.item.id);
		}
	}

	render() {

		return (
			<div className='content'>
				<span>{this.props.item.id}</span>
				{!this.props.item.done
					? <span>loading...</span>
					: <span className='green'>OK</span>
				}
			</div>
		);
	}
}

const mapStateToProps = ({ myStore }) => {
	return { store: myStore };
};

const mapDispatchToProps = dispatch => {
	return {
		uploadServices(index) {
			dispatch(setHandler(index));
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Content);
