import React from "react";
import './error-input.css'

const ErrorInput = props => {
	return (
		<div className="error-input">{props.error}</div>
	);
};

export default ErrorInput
