import React, { Component } from "react";
import { connect }          from "react-redux";
import "./list.css";
import Content              from "../content/content";

class List extends Component {
	render() {
		let isArray = Array.isArray(this.props.store);
		let limit   = isArray && this.props.store.length <= 5;
		let element = null;

		if (isArray && limit) {
			element = this.props.store.map((item) => {
				return (
					<li key={item.id}>
						<Content item={item}/>
					</li>
				);
			});
		} else if (!this.props.store) {
			element = null;
		}

		return (
			<ul className="list">
				{
					element
				}
			</ul>
		);
	}
}

function mapStateToProps({ myStore }) {
	return { store: myStore };
}

export default connect(mapStateToProps)(List);
