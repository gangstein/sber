import React, { Component, createRef } from "react";
import { connect }                     from "react-redux";
import nextTick                        from "next-tick";

import changedInput          from "../../handlers/changedInput";
import servicesLoadedHandler from "../../handlers/servicesLoadedHandler";

import Greeting   from "../greeting/greeting";
import List       from "../list/list";
import ErrorInput from "../errors/errorInput";

import "./app.css";

class App extends Component {
	constructor(props) {
		super(props);
		this.input = createRef();
		this.state = {
			error: {
				value:   false,
				message: null
			}
		};
	}

	getServices = () => {
		this.props.deleteServices();
		nextTick(() => this.props.setServices(this.input.current.value));
	};

	render() {

		return (
			<div className="app">
				<h3>введите целое число</h3>
				<input
					onChange={(e) => changedInput(e, this)}
					type="text"
					ref={this.input}/>
				<button
					disabled={this.state.error.value
								|| servicesLoadedHandler(this.props.store).disableButton}
					onClick={this.getServices}>
					send
				</button>
				{
					this.state.error.value
						? <ErrorInput error={this.state.error.message}/>
						: null
				}
				<List/>
				{servicesLoadedHandler(this.props.store).done ? <Greeting/> : null}
			</div>
		);
	}
}

const mapStateToProps = ({ myStore }) => {
	return { store: myStore };
};

const mapDispatchToProps = dispatch => {
	return {
		setServices(v) {
			dispatch({ type: "SET_SERVICES", payload: v });
		},
		deleteServices() {
			dispatch({ type: "DELETE_SERVICES" });
		}
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
