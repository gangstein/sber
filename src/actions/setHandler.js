export default function setHandler(index) {
	return dispatch => {
		let timeout =
			    Math.floor(Math.random() * ( 3000 - 1000 + 1 )) + 1000;

		setTimeout(() => {
			dispatch({ type: "UPLOAD_SERVICES", payload: index });
		}, timeout);
	};
}
