const every = (item) => {
	return item.done;
};

const servicesLoadedHandler = (item) => {
	if (Array.isArray(item)) {
		return {
			disableButton: !item.every(every),
			done: item.every(every)
		}
	} else {
		return {
			disableButton: false,
			done: false
		}
	}
};

export default servicesLoadedHandler;
