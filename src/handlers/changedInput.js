const changedInput = (e, context) => {
	let result = parseInt(e.currentTarget.value);
	if (!isNaN(result) && result > 5) {
		context.setState(
			{
				error:
					{
						value:   true,
						message: "значение должно быть меньше 6"
					}
			});
	} else if (!isNaN(result) && result < 1) {
		context.setState(
			{
				error:
					{
						value:   true,
						message: "значение должно быть больше 1"
					}
			});
	} else {
		context.setState(
			{
				error:
					{
						value:   false,
						message: null
					}
			});
	}
};

export default changedInput
