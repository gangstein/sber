import {
	combineReducers,
	createStore,
	applyMiddleware
}                              from "redux";
import thunk                   from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import myStore from "./redusers/myStore";


const store = createStore(
	combineReducers({ myStore }),
	composeWithDevTools(applyMiddleware(thunk))
);

export default store;
