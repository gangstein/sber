const initialState = null;

export default function myStore(store = initialState, action) {
	switch (action.type) {
		case "DELETE_SERVICES":
			return null;
		case "SET_SERVICES":
			const number = parseInt(action.payload.trim());
			let result   = null;
			if (!isNaN(number)) {
				result = [];
				for (let i = 1; i <= number; i++) {
					result.push({
						id:   i,
						done: false
					});
				}
			} else {
				result = null;
			}
			return result;
		case "UPLOAD_SERVICES": {
			let newStore = store.slice();
			newStore.forEach(item => {
				if (item.id === action.payload) {
					item.done = true
				}
			});
			return newStore;
		}
		default:
			return store;
	}
}
